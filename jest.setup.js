const dotenv = require("dotenv");
const Enzyme = require("enzyme");
const EnzymeAdapter = require("enzyme-adapter-react-16");

dotenv.config();

Enzyme.configure({ adapter: new EnzymeAdapter() });
